export async function get({ params }) {
  const user = {
    avatar: `https://avatars.dicebear.com/api/micah/${params.username}.svg`,
    username: params.username
  };

  return {
    body: user
  };
}