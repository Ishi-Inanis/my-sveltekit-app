export default interface Link {
  name: string;
  href: string;
  icon?: string;
}