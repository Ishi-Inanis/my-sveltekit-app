import type Link from '$lib/interfaces/link';

export const links: Link[] = [{
  name: 'Главная',
  href: '/'
}, {
  name: 'О нас',
  href: '/about'
}, {
  name: 'Профили',
  href: '/profile'
}];